# leetcode 刷题

## 代码使用 C++ 实现

> 格式：

```txt
题目会按类型进行分类：
  - array
    - basic(基础)
    - middle(中等)
    - difficulty(困难)
  - stack
    - basic(基础)
    - middle(中等)
    - difficulty(困难)
每一个题目文件目录：
  - demo_1
    - bin
      - main.out
    - solution
      - solution_1.cpp
      - solution_2.cpp
    - main.cpp
题目： xxxxx
举例： xxxxx
solution: xxxx
```
